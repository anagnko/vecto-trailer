![](https://notes.oss-labs.eu/uploads/9b824ff4-c385-4b59-8cf0-c594693822a4.JPG)

# VECTO Trailer release notes
## December 2023 - VECTOTrailer-1.0.1.3275

Here is what is new in the VECTO Trailer release:

- [Issue #20](https://code.europa.eu/vecto/vecto-trailer/-/issues/20): Corrected naming for XSD namespaces and files.
    - Use ***urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:Trailer:v1.0***
instead of ***urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:DEV:v2.7***
(See samples in folder "Generic Vehicles\Declaration Mode\Trailer\GenericTrailers")

- [Issue #30](https://code.europa.eu/vecto/vecto-trailer/-/issues/30): Fixed O3/O4 classification in relation to mass.
    - O3/O4 classification was wrongfully checked against total mass. Now it is correctly checked against TPMLM axle assembly.

- [Issue #32](https://code.europa.eu/vecto/vecto-trailer/-/issues/32): Hashing Tool fails to validate reports
    - Hashing Tool was looking for Tyre hash and certification number in the wrong place and could not verify the job data in the CIF. The Hashing Tool has been fixed and the VECTO simulator now includes the missing signature in the CIF.

- [Issue #33](https://code.europa.eu/vecto/vecto-trailer/-/issues/33): Trailer MRF & CIF XSDs version set to 1
    - Now that the Trailer tool has come out of the testing phase and is heading to official use, the XSDs version is set to 1.

- [Issue #34](https://code.europa.eu/vecto/vecto-trailer/-/issues/34): Improved Trailer Tool testing
    - Added tests for reference jobs, in order to allow more thorough automated testing. Categorized tests by the "Trailer" category to allow easy identification of relevant tests.

## October 2023 - VECTOTrailer-1.0.0.3198
This is the first official release of the tool produced by the JRC. 

Here is what is new in the VECTO Trailer release:

- [Issue #1](https://code.europa.eu/vecto/vecto-trailer/-/issues/1): Add support for Tyre component data in schema version 2.5
  - A new xml schema for tyre XMLs has been added to the official VECTO version. The new schema is now supported in the trailer tool as well. The Hashing tool has also been updated to support the new xml schema for tyres.

- [Issue #7](https://code.europa.eu/vecto/vecto-trailer/-/issues/7): Using cli cmd does not generate PDFs
  - The new input argument [-pdf] was implemented for the command line tool, in order to trigger the generation of pdf customer and manufacturer reports.

- [Issue #15](https://code.europa.eu/vecto/vecto-trailer/-/issues/15): Wrong reference trailers DBvol and DCvol
  - The MasterExcel was updated to reflect the actual reference trailer values.

## September 2023 - VECTOTrailer-0.9.0.3143
This is the first beta release of the tool produced by the JRC. It serves mainly as a pilot to the process, nevertheless incorporating new features which are essential to the completeness of the tool. Further releases will follow in September and October of 2023.

Here is what is new in this VECTO Trailer release:

- [Issue #12](https://code.europa.eu/vecto/vecto-trailer/-/issues/12): Remove tyre fuel efficiency label calculation in VECTO Trailer
  - If the tyre efficiency label is missing in the input, it will be not calculated for reporting.

- [Issue #10](https://code.europa.eu/vecto/vecto-trailer/-/issues/10): Update standard values for aero devices
  - The standard values for aero devices have been updated after their final definition for all trailers.
  - The same is true for volume oriented trailers.
  - All values are documented in the MasterExcel and the specific_trailer.csv
- [Issue #13](https://code.europa.eu/vecto/vecto-trailer/-/issues/13): Trailer aero reduction at 0deg yaw
  - The aero reduction for standard devices for 0deg yaw angle was hard coded to 0 and not read from the generic data.
- [Issue #8](https://code.europa.eu/vecto/vecto-trailer/-/issues/8): Add standard values RRC VECTO trailer
  - The standard values for RRC are now documented within the MasterExcel file.
- [Issue #2](https://code.europa.eu/vecto/vecto-trailer/-/issues/2): Side covers and rear flaps options should be mutually exclusive
  - On the GUI a mutually checkbox for the subtype of side covers and rear flaps was implemented, so that users cannot select both  at the same time.
- [Issue #3](https://code.europa.eu/vecto/vecto-trailer/-/issues/3): TPMLM and legislative category checks not correct for O3 and O4 vehicles
    - It is now not possible anymore to declare an O3 chategory trailer with a TPMLM higher than 10t and an O4 with a TPMLM lower than 10t.
    - It is now possible to declare O4 trailers with a single axle.
- [Issue #5](https://code.europa.eu/vecto/vecto-trailer/-/issues/5): Check the external height of the trailer body when declaring volume oriented trailers
  - A plausibility check on the external hight of the body was introduced to avoid wrong declaration of volume oriented trailers. No check was applied before. 

## August 2022 - VECTOTrailer-0.9.0.2799
This is the last beta release produced by [TUG](https://www.itna.tugraz.at/vecto/) based on the prior work of [emisia](https://www.emisia.com/). The tool is at this point deemed feature complete. 
