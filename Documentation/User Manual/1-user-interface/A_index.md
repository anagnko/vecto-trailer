User Interface
==============

When VECTO starts the [Main Form](#main-form) is loaded. Closing this form will close VECTO even if other dialogs are still open.

-	[Main Form](#main-form)
-   [Settings](#settings)
-	[Job Editor](#job-editor)
-   [Aux Dialog](#auxiliary-dialog)
-   [Advanced Auxiliary Dialog](#advanced-auxiliary-dialog)
-	[Vehicle Editor](#vehicle-editor)
-	[Engine Editor](#engine-editor)
-	[Gearbox Editor](#gearbox-editor)
-	[Graph Window](#graph-window)

