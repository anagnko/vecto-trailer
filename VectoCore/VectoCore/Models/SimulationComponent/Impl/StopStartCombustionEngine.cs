﻿using TUGraz.VectoCommon.Exceptions;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCommon.Utils;
using TUGraz.VectoCore.Models.Connector.Ports.Impl;
using TUGraz.VectoCore.Models.Declaration;
using TUGraz.VectoCore.Models.Simulation;
using TUGraz.VectoCore.Models.Simulation.Data;
using TUGraz.VectoCore.Models.SimulationComponent.Data;
using TUGraz.VectoCore.OutputData;
using TUGraz.VectoCore.Utils;

namespace TUGraz.VectoCore.Models.SimulationComponent.Impl {
	public class StopStartCombustionEngine : CombustionEngine
	{
		protected double EngineStopStartUtilityFactor;
		private WattSecond EngineStartEnergy;

		public StopStartCombustionEngine(
			IVehicleContainer container, CombustionEngineData modelData, bool pt1Disabled = false) : base(
			container, modelData, pt1Disabled)
		{
			IgnitionOn = true;
			EngineStopStartUtilityFactor = container.RunData.DriverData.EngineStopStart.UtilityFactor;

			var engineRampUpEnergy = Formulas.InertiaPower(modelData.IdleSpeed, 0.RPMtoRad(), modelData.Inertia, modelData.EngineStartTime) * modelData.EngineStartTime;
			var engineDragEnergy = VectoMath.Abs(modelData.FullLoadCurves[0].DragLoadStationaryTorque(modelData.IdleSpeed)) *
									modelData.IdleSpeed / 2.0 * modelData.EngineStartTime;

			EngineStartEnergy = (engineRampUpEnergy + engineDragEnergy) * EngineStopStartUtilityFactor / DeclarationData.AlternaterEfficiency / DeclarationData.AlternaterEfficiency;
		}

		public override bool IgnitionOn { get; set; }

		#region Overrides of CombustionEngine

		public override IResponse Request(Second absTime, Second dt, NewtonMeter outTorque, PerSecond outAngularVelocity, bool dryRun)
		{
			return IgnitionOn ? base.Request(absTime, dt, outTorque, outAngularVelocity, dryRun) : HandleEngineOffRequest(absTime, dt, outTorque, outAngularVelocity, dryRun);
		}

		#endregion

		protected virtual IResponse HandleEngineOffRequest(Second absTime, Second dt, NewtonMeter outTorque, PerSecond outAngularVelocity, bool dryRun)
		{
			CurrentState.IgnitionOn = false;
			CurrentState.EngineSpeed = ModelData.IdleSpeed;
			CurrentState.EngineTorque = 0.SI<NewtonMeter>();
			CurrentState.EnginePower = 0.SI<Watt>();
			CurrentState.dt = dt;

			if (!dryRun) {
				//EngineAux.TorqueDemand(absTime, dt, 0.SI<NewtonMeter>(), 0.SI<NewtonMeter>(), ModelData.IdleSpeed);
				CurrentState.AuxPowerEngineOff = EngineAux.PowerDemandEngineOff(absTime, dt);
			} else {
				return new ResponseDryRun {
					DeltaFullLoad = 0.SI<Watt>(),
					DeltaDragLoad = 0.SI<Watt>(),
					EngineTorqueDemandTotal = 0.SI<NewtonMeter>(),
					DeltaEngineSpeed = 0.RPMtoRad(),
					EnginePowerRequest = 0.SI<Watt>(),
					DynamicFullLoadPower = 0.SI<Watt>(),
					DragPower = 0.SI<Watt>(),
					AuxiliariesPowerDemand = 0.SI<Watt>(),
					EngineSpeed = 0.RPMtoRad(),
					Source = this,
				};
			}

			return new ResponseSuccess() {
				EnginePowerRequest = 0.SI<Watt>(),
				DynamicFullLoadPower = 0.SI<Watt>(),
				EngineTorqueDemandTotal = 0.SI<NewtonMeter>(),
				DragPower = 0.SI<Watt>(),
				AuxiliariesPowerDemand = 0.SI<Watt>(),
				EngineSpeed = 0.RPMtoRad(),
				Source = this
			};
		}

		#region Overrides of CombustionEngine

		protected override void DoWriteModalResults(Second time, Second simulationInterval, IModalDataContainer container)
		{
			if (IgnitionOn) {
				base.DoWriteModalResults(time, simulationInterval, container);
				var engineStart = !PreviousState.IgnitionOn && CurrentState.IgnitionOn;
				container[ModalResultField.P_ice_start] = engineStart ? EngineStartEnergy / CurrentState.dt : 0.SI<Watt>();
				container[ModalResultField.P_aux_ice_off] = 0.SI<Watt>();
			} else {
				container[ModalResultField.P_ice_start] = 0.SI<Watt>();
				DoWriteEngineOffResults(time, simulationInterval, container);
			}

		}

		#endregion

		protected virtual void DoWriteEngineOffResults(Second time, Second simulationInterval, IModalDataContainer container)
		{
			container[ModalResultField.P_ice_fcmap] = 0.SI<Watt>();
			container[ModalResultField.P_ice_out] = 0.SI<Watt>();
			container[ModalResultField.P_ice_inertia] = 0.SI<Watt>();

			container[ModalResultField.n_ice_avg] = 0.RPMtoRad();
			container[ModalResultField.T_ice_fcmap] = 0.SI<NewtonMeter>();

			container[ModalResultField.P_ice_full] = 0.SI<Watt>();
			container[ModalResultField.P_ice_full_stat] = 0.SI<Watt>();
			container[ModalResultField.P_ice_drag] = 0.SI<Watt>();
			container[ModalResultField.T_ice_full] = 0.SI<NewtonMeter>();
			container[ModalResultField.T_ice_drag] = 0.SI<NewtonMeter>();

			container[ModalResultField.ICEOn] = CurrentState.IgnitionOn;
			container[ModalResultField.P_aux_ice_off] = (CurrentState.AuxPowerEngineOff ?? 0.SI<Watt>());


			var auxDemand = EngineAux.PowerDemandEngineOn(time, simulationInterval, ModelData.IdleSpeed) / ModelData.IdleSpeed;

			WriteWHRPower(container, ModelData.IdleSpeed, auxDemand);

			foreach (var fuel in ModelData.Fuels) {
				var fc = 0.SI<KilogramPerSecond>();
				var fcNCVcorr = fc * fuel.FuelData.HeatingValueCorrection; // TODO: wird fcNCVcorr

				var fcWHTC = fcNCVcorr * WHTCCorrectionFactor(fuel.FuelData);
				//var fcAAUX = fcWHTC;
				var advancedAux = EngineAux as BusAuxiliariesAdapter;
				if (advancedAux != null) {
					//throw new VectoException("Engine Stop/Start with advanced auxiliaries not supported!");
					advancedAux.DoWriteModalResults(time, simulationInterval, container);
					//fcAAUX = advancedAux.AAuxFuelConsumption;
				}

				
				var result = fuel.ConsumptionMap.GetFuelConsumption(auxDemand, ModelData.IdleSpeed);

				var fcESS = result.Value * (1 - EngineStopStartUtilityFactor) * fuel.FuelData.HeatingValueCorrection * WHTCCorrectionFactor(fuel.FuelData);
				var fcFinal = fcESS;

				container[ModalResultField.FCMap, fuel.FuelData] = fc;
				container[ModalResultField.FCNCVc, fuel.FuelData] = fcNCVcorr;
				container[ModalResultField.FCWHTCc, fuel.FuelData] = fcWHTC;
				//container[ModalResultField.FCAAUX, fuel.FuelData] = fcAAUX;
				container[ModalResultField.FCICEStopStart, fuel.FuelData] = fcESS;
				container[ModalResultField.FCFinal, fuel.FuelData] = fcFinal;
			}
		}

		protected override void WriteWHRPower(IModalDataContainer container, PerSecond engineSpeed, NewtonMeter engineTorque)
		{
			var pWHRelMap = 0.SI<Watt>();
			var pWHRelCorr = 0.SI<Watt>();
			var pWHRmechMap = 0.SI<Watt>();
			var pWHRmechCorr = 0.SI<Watt>();
			GetWHRPower(ModelData.ElectricalWHR, engineSpeed, engineTorque, ref pWHRelMap, ref pWHRelCorr);
			GetWHRPower(ModelData.MechanicalWHR, engineSpeed, engineTorque, ref pWHRmechMap, ref pWHRmechCorr);

			container[ModalResultField.P_WHR_el_map] = (1 - EngineStopStartUtilityFactor) * pWHRelMap;
			container[ModalResultField.P_WHR_el_corr] = (1 - EngineStopStartUtilityFactor) * pWHRelCorr;

			container[ModalResultField.P_WHR_mech_map] = (1 - EngineStopStartUtilityFactor) * pWHRmechMap;
			container[ModalResultField.P_WHR_mech_corr] = (1 - EngineStopStartUtilityFactor) * pWHRmechCorr;
		}
	}
}