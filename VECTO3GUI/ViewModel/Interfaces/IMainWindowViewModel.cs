﻿namespace VECTO3GUI.ViewModel.Interfaces
{
	public interface IMainWindowViewModel
	{
		IMainView CurrentViewModel { get; }
		string Version { get; }
	}
}
